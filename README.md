# Maze game for HTC Vive VR headset
A simple level based maze game using Unity 5.5 where the player has to find a way through the maze. The player can collect various coins present in the maze which will add to his score. Also, there exists some enemies, collision with which will end the game. We have implemented two levels after which the end game screen is displayed with the final scores of the player.  The player gets a game over or game completed screen from which he can restart the game or quit the application. In every level of the game the player needs to find way to the exit door open the door by touching the door knob and pressing trigger on right controller and collecting the key on the other end of the door will take the player to next level or game completed screen when all the levels are done.

# Requirements
1. HTC Vive VR headset
2. Unity 5.5

# Running the game
    Run the provided Project_VR.exe
You can walk around as a first person controller using HTC vive controllers touchpad. Just press front on the touchpad and you can walk in the direction of the headset/gaze. Find the exit door, touch door knob and press the trigger with right controller. You can then collect the key in front of you to reach next level. The key and coins can be collected just by touching with the controllers.

# Screenshots
## Level 1 initial screen
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/level1_initial_screen.jpg" width="700" height="400" align="center"/>

## Coin Collection
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/coin_collection.jpg" width="700" height="400" align="center"/>

## Score updation
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/score.jpg" width="700" height="400" align="center"/>

## Enemy
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/enemy.jpg" width="700" height="400" align="center"/>

## Game over screen
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/game_over_screen.jpg" width="700" height="400" align="center"/>

## Exit door
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/exit_door.jpg" width="700" height="400" align="center"/>

## Score board
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/score_board.jpg" width="700" height="400" align="center"/>

## Level 2 initial screen and score preservation from previous level
<img src="https://github.com/rachhshruti/maze-game-vr/blob/master/images/level2_initial_screen.jpg" width="700" height="400" align="center"/>

# Authors
* Shruti Rachh
* Prerna Preeti
